import os
import requests
import re
import sys
from bs4 import BeautifulSoup, Tag, NavigableString


class ProspectiveArticle:
    def __init__(self, div: Tag):
        self.div = div
        self.char_count = self.get_char_count()

    def get_char_count(self):
        char_count = 0
        for item in self.div.contents:
            if type(item) == NavigableString:
                char_count += len(item)

        text_attributes = self.div.find_all(['p', 'b', 'li'], recursive=False)
        for text_attribute in text_attributes:
            for item in text_attribute.contents:
                if type(item) == NavigableString:
                    char_count += len(item)

        return char_count


class Article:
    def __init__(self, div: Tag, src: str):
        self.div = div
        self.src = self.get_src(src)
        self.header = self._get_header()
        self.formatted_text = ''

    @staticmethod
    def get_src(src: str):
        if src[-1] == '/':
            return src[:-1]
        return src

    def get_article_text(self):
        return self.div.get_text()

    def get_header_text(self):
        if self.header is None:
            return ''
        return self.header.get_text()

    @staticmethod
    def _rec_header_finder(div: Tag):
        parent = div.parent
        if parent is None:
            return None
        h1 = parent.find('h1')
        if h1 is not None:
            return h1
        return Article._rec_header_finder(parent)

    def _get_header(self):
        parent = self.div.parent
        header = parent.find('h1')
        if header is None:
            return Article._rec_header_finder(parent)
        return header


class ArticleFormatter:
    def __init__(self, article: Article):
        self.article = article

    @staticmethod
    def _format_by_length(item):
        all_text = item
        if len(all_text) > 80:
            split_text = all_text.split(' ')
            text_lines = []
            text_line = ''
            for word in split_text:
                if len(' '.join([text_line, word])) > 80:
                    text_line = text_line.lstrip()
                    text_lines.append(text_line)
                    text_line = ''
                    text_line = ' '.join([text_line, word])
                else:
                    text_line = ' '.join([text_line, word])

            if len(text_line) <= 80:
                text_line = text_line.lstrip()
                text_lines.append(text_line)

            all_text = '\n'.join(text_lines)

        return all_text + '\n'

    def get_formatted_text(self):
        formatted_text = ''
        for item in self.article.div.contents:
            if type(item) == NavigableString:
                formatted_text += self._format_by_length(item) + '\n'
            if item.name in ['p', 'b', 'i', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6']:
                formatted_text += self._format_by_length(item.get_text()) + '\n'
            if item.name == 'ul':
                lis = item.find_all('li')
                for li in lis:
                    formatted_text += '   {}'.format(self._format_by_length(li.get_text())) + '\n'
            if item.name == 'a':
                a_text = item.get_text()
                if a_text != ('' or ' '):
                    try:
                        formatted_a = ' [{}]{} '.format(item['href'], self._format_by_length(a_text))
                        formatted_text = '{} {}'.format(formatted_text[:-3],
                                                        self._format_by_length(formatted_a)[:-2].lstrip())
                    except KeyError:
                        pass

        formatted_text = '{}\n\n{}'.format(self.article.get_header_text(), formatted_text)
        formatted_text = re.sub(r'(\s*\n){3,10}', r'\n\n', formatted_text)

        return formatted_text.lstrip()


class ArticleWriter:
    def __init__(self, article: Article):
        self.article = article
        self.path = self._generate_path()

    def _generate_path(self):
        split_src = self.article.src.split('/')
        file_name = split_src.pop(-1)
        dir_path = '/'.join(split_src)
        try:
            os.makedirs(dir_path)
        except FileExistsError:
            pass

        split_file_name = file_name.split('.')
        if split_file_name:
            file_name = '{}.txt'.format(split_file_name[0])

        return '{}/{}'.format(dir_path, file_name)

    def write_to_document(self):
        with open(self.path, 'w') as file:
            file.write('{}'.format(self.article.formatted_text))


def find_divs(soup: BeautifulSoup):
    return soup.find_all('div')


def sort_by_scope(prospective_article: ProspectiveArticle):
    return prospective_article.char_count


def get_article_from_divs(divs: Tag):
    prospective_articles = [ProspectiveArticle(div) for div in divs]
    prospective_articles.sort(key=sort_by_scope, reverse=True)
    return prospective_articles[0].div


def get_formatted_text(article: Article):
    article_formatter = ArticleFormatter(article)
    return article_formatter.get_formatted_text()


if __name__ == '__main__':
    arg_src = sys.argv[1]
    r = requests.get(arg_src).content
    beautiful_soup_obj = BeautifulSoup(r, 'html.parser')
    divs = find_divs(beautiful_soup_obj)

    article_div = get_article_from_divs(divs)

    article = Article(article_div, arg_src)

    article.formatted_text = get_formatted_text(article)

    writer = ArticleWriter(article)
    writer.write_to_document()
